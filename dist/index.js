"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const alexa_wrapper_1 = require("./src/Alexa_Wrapper/alexa_wrapper");
const handler = (event, context) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("-------7. index.ts Received context from Alexa => ", JSON.stringify(event));
    let response = yield (0, alexa_wrapper_1.alexaWrapper)(event);
    return JSON.stringify(response);
});
exports.handler = handler;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntentMap = void 0;
const AnswerAction_1 = __importDefault(require("./src/business/Actions/AnswerAction"));
const CategoryAction_1 = __importDefault(require("./src/business/Actions/CategoryAction"));
const LaunchAction_1 = __importDefault(require("./src/business/Actions/LaunchAction"));
const QuizAction_1 = __importDefault(require("./src/business/Actions/QuizAction"));
const RepeatAction_1 = __importDefault(require("./src/business/Actions/RepeatAction"));
const TimeoutAction_1 = __importDefault(require("./src/business/Actions/TimeoutAction"));
const UserEventAction_1 = __importDefault(require("./src/business/Actions/UserEventAction"));
const YesAction_1 = __importDefault(require("./src/business/Actions/YesAction"));
exports.IntentMap = {
    LAUNCH: new LaunchAction_1.default(),
    USER_EVENT: new UserEventAction_1.default(),
    ANSWER_INTENT: new AnswerAction_1.default(),
    CHOOSE_CATEGORY_INTENT: new CategoryAction_1.default(),
    YES_INTENT: new YesAction_1.default(),
    QUIZ_INTENT: new QuizAction_1.default(),
    TIMEOUT_INTENT: new TimeoutAction_1.default(),
    UNKNOWN: new RepeatAction_1.default(),
    REPEAT: new RepeatAction_1.default()
};

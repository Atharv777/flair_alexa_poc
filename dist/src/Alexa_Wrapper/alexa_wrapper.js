"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.alexaWrapper = void 0;
const BusinessHandler_1 = require("../business/BusinessHandler");
const PostprocessingHandler_1 = require("../post_processing/PostprocessingHandler");
const preprocessing_1 = require("../preprocessing");
function alexaWrapper(event) {
    return __awaiter(this, void 0, void 0, function* () {
        let preprocessedData = yield (0, preprocessing_1.preprocessingHandler)(event);
        console.log("-----------8. alexa_wrapper.ts preprocessedData----------", JSON.stringify(preprocessedData));
        let businessOutput = yield (0, BusinessHandler_1.businessHandler)(preprocessedData);
        console.log("-----------11. alexa_wrapper.ts businessOutput----------", JSON.stringify(businessOutput));
        let alexaResponse = yield (0, PostprocessingHandler_1.postProcessingHandler)(businessOutput);
        console.log("-----------14. alexa_wrapper.ts postProcessedData----------", JSON.stringify(alexaResponse));
        return alexaResponse;
    });
}
exports.alexaWrapper = alexaWrapper;

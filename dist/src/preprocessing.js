"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.preprocessingHandler = void 0;
const lodash_1 = __importDefault(require("lodash"));
const IActionInput_1 = require("./IActionInput");
// enum DefaultActions {
//     // LEADERBOARD_FOR_STATE = "LEADERBOARD_FOR_STATE",
//     // NATION_LEADERBOARD = "NATION_LEADERBOARD",
//     CONNETION_RESPONSE = "CONNETION_RESPONSE",
//     BUY_INTENT = "BUY_INTENT",
//     REFUND_INTENT = "REFUND_INTENT",
//     SESSION_END = "SESSION_END",
//     UNKNOWN = "UNKNOWN",
//     END = "END",
//     HELP = "HELP",
//     // CONTINUE_GAME = "CONTINUE_GAME",
//     // PLAYER_STATISTICS = "PLAYER_STATISTICS",
//     LINK_ACCOUNT = "LINK_ACCOUNT",
//     SESSION_RESUMED_REQUEST = "SESSION_RESUMED_REQUEST",
//     CUSTOM_TASK = "CUSTOM_TASK",
//     WHAT_DID_I_BUY = "WHAT_DID_I_BUY",
//     WHAT_CAN_I_BUY = "WHAT_CAN_I_BUY",
//     LAUNCH = "LAUNCH",
//     YES = "YES",
//     NO = "NO",
//     REPEAT = "REPEAT",
//     // LEADERBOARD = "LEADERBOARD",
//     // ANSWER_INTENT = "ANSWER_INTENT",
//     // ASK_QUESTION = "ASK_QUESTION",
//     // USER_STATE_INTENT = "USER_STATE_INTENT",
//     NEXT = "NEXT",
//     USER_EVENT = "USER_EVENT",
//     // BC_HOME = "BC_HOME",
//     // AOT_HOME = "AOT_HOME",
//     // CM_HOME = "CM_HOME",
//     // MHA_HOME = "MHA_HOME"
// }
// let IntentMap: { [key: string]: DefaultActions } = {
//     "LaunchRequest": DefaultActions.LAUNCH,
//     // "LeaderboardIntent": DefaultActions.LEADERBOARD,
//     // "UserstateIntent": DefaultActions.USER_STATE_INTENT,
//     // "AnswerIntent": DefaultActions.ANSWER_INTENT,
//     // "QuestionIntent": DefaultActions.ASK_QUESTION,
//     "RepeatIntent": DefaultActions.REPEAT,
//     "AMAZON.YesIntent": DefaultActions.YES,
//     "AMAZON.NoIntent": DefaultActions.NO,
//     "AMAZON.NextIntent": DefaultActions.NEXT,
//     // "BlackCloverIntent": DefaultActions.BC_HOME,
//     // "AttackOnTitanIntent": DefaultActions.AOT_HOME,
//     // "ChainsawmanIntent": DefaultActions.CM_HOME,
//     // "MHAIntent": DefaultActions.MHA_HOME,
// }
var DefaultActions;
(function (DefaultActions) {
    DefaultActions["CONNETION_RESPONSE"] = "CONNETION_RESPONSE";
    DefaultActions["BUY_INTENT"] = "BUY_INTENT";
    DefaultActions["REFUND_INTENT"] = "REFUND_INTENT";
    DefaultActions["SESSION_END"] = "SESSION_END";
    DefaultActions["UNKNOWN"] = "UNKNOWN";
    DefaultActions["END"] = "END";
    DefaultActions["HELP"] = "HELP";
    DefaultActions["LINK_ACCOUNT"] = "LINK_ACCOUNT";
    DefaultActions["SESSION_RESUMED_REQUEST"] = "SESSION_RESUMED_REQUEST";
    DefaultActions["CUSTOM_TASK"] = "CUSTOM_TASK";
    DefaultActions["WHAT_DID_I_BUY"] = "WHAT_DID_I_BUY";
    DefaultActions["WHAT_CAN_I_BUY"] = "WHAT_CAN_I_BUY";
    DefaultActions["LAUNCH"] = "LAUNCH";
    DefaultActions["YES"] = "YES_INTENT";
    DefaultActions["NO"] = "NO";
    DefaultActions["REPEAT"] = "REPEAT";
    DefaultActions["NEXT"] = "NEXT";
    DefaultActions["USER_EVENT"] = "USER_EVENT";
})(DefaultActions || (DefaultActions = {}));
let defaultIntentMap = {
    "LaunchRequest": DefaultActions.LAUNCH,
    "RepeatIntent": DefaultActions.REPEAT,
    "AMAZON.YesIntent": DefaultActions.YES,
    "AMAZON.NoIntent": DefaultActions.NO,
    "AMAZON.NextIntent": DefaultActions.NEXT,
    "AMAZON.CONNETION_RESPONSE": DefaultActions.CONNETION_RESPONSE,
    "AMAZON.BUY_INTENT": DefaultActions.BUY_INTENT,
    "AMAZON.REFUND_INTENT": DefaultActions.REFUND_INTENT,
    "AMAZON.SESSION_END": DefaultActions.SESSION_END,
    "AMAZON.UNKNOWN": DefaultActions.UNKNOWN,
    "AMAZON.END": DefaultActions.END,
    "AMAZON.HELP": DefaultActions.HELP,
    "AMAZON.LINK_ACCOUNT": DefaultActions.LINK_ACCOUNT,
    "AMAZON.SESSION_RESUMED_REQUEST": DefaultActions.SESSION_RESUMED_REQUEST,
    "AMAZON.CUSTOM_TASK": DefaultActions.CUSTOM_TASK,
    "AMAZON.WHAT_DID_I_BUY": DefaultActions.WHAT_DID_I_BUY,
    "AMAZON.WHAT_CAN_I_BUY": DefaultActions.WHAT_CAN_I_BUY,
    "Alexa.Presentation.APL.UserEvent": DefaultActions.USER_EVENT,
    "IntentRequest": DefaultActions.USER_EVENT
};
let preprocessingHandler = (event) => __awaiter(void 0, void 0, void 0, function* () {
    const userContext = getUserContext(event);
    const sessionContext = getSessionContext(event);
    const deviceContext = getDeviceContext(event);
    const requestContext = getRequestContext(event);
    userContext.inSkillProducts = yield getInskillProducts();
    let { verb, requestData } = parseRequestData(event);
    requestContext.data = requestData;
    let actionInput = {
        verb: verb,
        platForm: "Alexa",
        userContext: userContext,
        sessionContext: sessionContext,
        deviceContext: deviceContext,
        requestContext: requestContext
    };
    // console.log("--------(119)Preprocessed data->", JSON.stringify(actionInput))
    return actionInput;
});
exports.preprocessingHandler = preprocessingHandler;
function getInskillProducts() {
    return __awaiter(this, void 0, void 0, function* () { return []; });
}
function parseRequestData(event) {
    let requestData = {};
    let verb = getVerb(event);
    lodash_1.default.set(requestData, "arguments", lodash_1.default.get(event, "request.arguments") || []);
    lodash_1.default.set(requestData, "slots", parseSlotValues(lodash_1.default.get(event, "request.intent.slots") || lodash_1.default.get(event, "request.arguments[0]['SLOTS']") || {}));
    return { verb, requestData };
}
function getVerb(event) {
    let requestType = lodash_1.default.get(event, "request.type");
    let verb = defaultIntentMap[requestType] || lodash_1.default.get(event, "request.intent.name") || DefaultActions.UNKNOWN;
    if (verb === 'USER_EVENT') {
        if (lodash_1.default.has(event.request, "arguments[0]['args']['EventName']")) {
            console.log("----------IntentName is inside event.request.arguments[0].args.EventName---------", event.request.arguments[0].args.EventName);
            verb = event.request.arguments[0].args.EventName;
        }
        else if (lodash_1.default.has(event.request, "intent")) {
            console.log("--------IntentName is inside event.request.intent.name------------", event.request.intent.name);
            verb = event.request.intent.name === "CHOOSE_CATEGORY_INTENT" ? "QUIZ_INTENT" : event.request.intent.name;
        }
        else if (lodash_1.default.has(event.request, "arguments[0]['EventName']")) {
            console.log("---------IntentName is inside event.request.arguments[0]['EventName']----------", event.request.arguments[0]['EventName']);
            verb = event.request.arguments[0]['EventName'];
        }
    }
    return verb;
}
function getUserContext(event) {
    return {
        userId: lodash_1.default.get(event, "session.user.userId"),
        inSkillProducts: [],
        userAttributes: lodash_1.default.get(event, "userAttributes") || {}
    };
}
function getSessionContext(event) {
    return {
        sessionId: lodash_1.default.get(event, "session.sessionId"),
        sessionAttributes: lodash_1.default.get(event, "session.attributes"),
        apiAccessToken: lodash_1.default.get(event, "context.System.apiAccessToken"),
        apiEndpoint: lodash_1.default.get(event, "context.System.apiEndpoint"),
        skillId: lodash_1.default.get(event, "context.System.application.applicationId"),
        isNewSession: lodash_1.default.get(event, "session.new")
    };
}
function getDeviceContext(event) {
    let interfaceReferenceMap = {
        "AudioPlayer": IActionInput_1.AlexaInterfaces.AUDIO_PLAYER,
        "Alexa.Presentation.APL": IActionInput_1.AlexaInterfaces.APL
    };
    let supportedInterfaces = lodash_1.default.keys(lodash_1.default.get(event, "context.System.device.supportedInterfaces") || {})
        .map((e) => { return interfaceReferenceMap[e] || null; })
        .filter((e) => e != null);
    return {
        deviceId: lodash_1.default.get(event, "context.System.device.deviceId"),
        shape: lodash_1.default.get(event, "context.Viewport.shape"),
        mode: lodash_1.default.get(event, "context.Viewport.mode"),
        dpi: lodash_1.default.get(event, "context.Viewport.dpi"),
        width: lodash_1.default.get(event, "context.Viewport.currentPixelWidth"),
        height: lodash_1.default.get(event, "context.Viewport.currentPixelHeight"),
        isDisplayEnabled: supportedInterfaces.includes(IActionInput_1.AlexaInterfaces.APL),
        supportedInterfaces
    };
}
function getRequestContext(event) {
    return {
        requestId: lodash_1.default.get(event, "request.requestId"),
        timestamp: lodash_1.default.get(event, "request.timestamp"),
        locale: lodash_1.default.get(event, "request.locale"),
        data: {}
    };
}
function parseSlotValues(slotData) {
    let slots = [];
    let slotObjKeys = Object.keys(slotData);
    if (slotObjKeys.includes("category") || slotObjKeys.includes("boolean") || slotObjKeys.includes("answer_option") || slotObjKeys.includes("answer_value") || slotObjKeys.includes("user_stat")) {
        console.log("---------212. preprocessing.ts slotData----------", slotData);
        lodash_1.default.keys(slotData || {}).forEach((e) => {
            let element = slotData[e] || {};
            if (lodash_1.default.has(element, "resolutions")) {
                slots.push({
                    slotValue: lodash_1.default.get(element, "value") || lodash_1.default.get(element, "resolutions.resolutionsPerAuthority[0].values[0].value.name"),
                    slotId: lodash_1.default.get(element, "resolutions.resolutionsPerAuthority[0].values[0].value.id"),
                    slotName: lodash_1.default.get(element, "name") || e,
                    test: "Speech Event"
                });
            }
        });
        return slots;
    }
    else {
        console.log("---------226. preprocessing.ts slotData----------", slotData);
        slots.push({
            slotValue: slotData['SLOT_VALUE'],
            slotId: slotData['SLOT_ID'],
            slotName: slotData['SLOT_NAME'],
            test: "Touch Event"
        });
        return slots;
    }
}

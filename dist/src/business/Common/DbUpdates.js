"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateUserLastQuestion = exports.updateRounds = exports.getRoundsPlayed = exports.updateUserData = exports.databaseGet = void 0;
const Utils_1 = require("./Utils");
function databaseGet(user_id, context) {
    return new Promise((resolve, reject) => {
        context.dao.getById("trivia-rush-user-data", { user_id: user_id }, (data) => {
            resolve(data);
        });
    });
}
exports.databaseGet = databaseGet;
function updateUserData(user_id, total_score, last_score, round, rounds_played, context) {
    let data = {
        user_id: user_id,
        last_question: null,
        total_score: total_score,
        last_score: last_score,
        round: round,
        play_date: (0, Utils_1.formatDate)(new Date()),
        rounds_played: rounds_played
    };
    // console.log("--------update user data---------", data)
    return new Promise((resolve, reject) => {
        context.dao.save("trivia-rush-user-data", data, (data) => {
            resolve(data);
        });
    });
}
exports.updateUserData = updateUserData;
function getRoundsPlayed(user_id, context) {
    return __awaiter(this, void 0, void 0, function* () {
        let user = yield Promise.all([databaseGet(user_id, context)]);
        if (user[0]) {
            return user[0].rounds_played;
        }
    });
}
exports.getRoundsPlayed = getRoundsPlayed;
function updateRounds(user_id, context) {
    return __awaiter(this, void 0, void 0, function* () {
        let user = yield Promise.all([databaseGet(user_id, context)]);
        if (user[0].play_date === (0, Utils_1.formatDate)(new Date()) && user[0].round <= 3) {
            let data = {
                user_id: user_id,
                round: user[0].round + 1,
                rounds_played: user[0].round,
                play_date: (0, Utils_1.formatDate)(new Date()),
                last_question: null,
                total_score: user[0].total_score,
                last_score: 0
            };
            // console.log("--------update rounds---------", data)
            return new Promise((resolve, reject) => {
                context.dao.save("trivia-rush-user-data", data, (data) => {
                    resolve(data);
                });
            });
        }
    });
}
exports.updateRounds = updateRounds;
function updateUserLastQuestion(user_id, questionSet, last_context, last_speech, context) {
    return __awaiter(this, void 0, void 0, function* () {
        let user = yield Promise.all([databaseGet(user_id, context)]);
        let data = {
            user_id: "",
            last_question: {
                answer: "",
                options: undefined,
                text: ""
            },
            total_score: 0,
            last_score: 0,
            round: 0,
            play_date: undefined,
            rounds_played: 0,
            last_context: "",
            last_speech: ""
        };
        if (user[0]) {
            if (user[0].play_date !== (0, Utils_1.formatDate)(new Date())) {
                data = {
                    user_id: user_id,
                    last_question: questionSet,
                    total_score: user[0].total_score,
                    last_score: 0,
                    round: 1,
                    play_date: (0, Utils_1.formatDate)(new Date()),
                    rounds_played: user[0].rounds_played,
                    last_context: last_context,
                    last_speech: last_speech
                };
                // console.log("--------different date---------", data)
            }
            else {
                data = {
                    user_id: user_id,
                    last_question: questionSet,
                    total_score: user[0].total_score,
                    last_score: user[0].last_score,
                    round: user[0].round,
                    play_date: (0, Utils_1.formatDate)(new Date()),
                    rounds_played: user[0].rounds_played,
                    last_context: last_context,
                    last_speech: last_speech
                };
                // console.log("--------same date---------", data)
            }
        }
        else {
            data = {
                user_id: user_id,
                last_question: questionSet,
                total_score: 0,
                last_score: 0,
                round: 1,
                play_date: (0, Utils_1.formatDate)(new Date()),
                rounds_played: 0,
                last_context: last_context,
                last_speech: last_speech
            };
            // console.log("--------new user---------", data)
        }
        return new Promise((resolve, reject) => {
            context.dao.save("trivia-rush-user-data", data, (data) => {
                resolve(data);
            });
        });
    });
}
exports.updateUserLastQuestion = updateUserLastQuestion;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatDate = exports.padTo2Digits = exports.sendEventHelper = void 0;
function sendEventHelper(preprocessedData) {
    if (preprocessedData.verb === "LAUNCH") {
        let sendEventObj = {
            type: "SendEvent",
            token: "image-no-text",
            arguments: [
                {
                    args: {
                        EventName: "CHOOSE_CATEGORY_INTENT"
                    }
                }
            ]
        };
        return sendEventObj;
    }
    return {};
}
exports.sendEventHelper = sendEventHelper;
function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
}
exports.padTo2Digits = padTo2Digits;
function formatDate(date) {
    return [
        date.getFullYear(),
        padTo2Digits(date.getMonth() + 1),
        padTo2Digits(date.getDate()),
    ].join('-');
}
exports.formatDate = formatDate;

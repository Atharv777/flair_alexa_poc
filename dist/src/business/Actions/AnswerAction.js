"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const QuizAction_1 = __importDefault(require("./QuizAction"));
const DbUpdates_1 = require("../Common/DbUpdates");
class AnswerAction {
    executeAction(preprocessedData, context) {
        return __awaiter(this, void 0, void 0, function* () {
            let slotValue = preprocessedData.requestContext.data.slots[0].slotValue;
            console.log("-----------9. AnswerAction.ts slotValue -------------", slotValue);
            let user = yield Promise.all([(0, DbUpdates_1.databaseGet)(preprocessedData.userContext.userId, context)]);
            // console.log("-----------12. AnswerAction.ts userData--------", user)
            if (user[0]) {
                let questionSet = user[0].last_question;
                let totalScore = user[0].total_score;
                let lastScore = user[0].last_score;
                if (slotValue.toLowerCase() === questionSet.answer.toLowerCase()) {
                    if (lastScore === 50) {
                        totalScore = totalScore + 100;
                        lastScore = 100;
                    }
                    else if (lastScore === 100 || lastScore === 150) {
                        totalScore = totalScore + 150;
                        lastScore = 150;
                    }
                    else {
                        totalScore = totalScore + 50;
                        lastScore = 50;
                    }
                }
                else {
                    totalScore = totalScore;
                    lastScore = 0;
                }
                yield Promise.all([(0, DbUpdates_1.updateUserData)(user[0].user_id, totalScore, lastScore, user[0].round, user[0].rounds_played, context)]);
            }
            else {
                yield Promise.all([(0, DbUpdates_1.updateUserData)(preprocessedData.userContext.userId, 50, 110, 1, 0, context)]);
            }
            const response = new QuizAction_1.default().executeAction(preprocessedData, context);
            return response;
        });
    }
}
exports.default = AnswerAction;

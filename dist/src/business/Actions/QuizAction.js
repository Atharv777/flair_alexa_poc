"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const Utils_1 = require("../Common/Utils");
const DbUpdates_1 = require("../Common/DbUpdates");
class QuizAction {
    executeAction(preprocessedData, context, isRepeat) {
        return __awaiter(this, void 0, void 0, function* () {
            let questionData = {};
            questionData = (lodash_1.default.has(preprocessedData.sessionContext.sessionAttributes, "category")) ? require(`../DataSources/${preprocessedData.sessionContext.sessionAttributes.category}.json`) : require(`../DataSources/${preprocessedData.requestContext.data.slots[0].slotValue.toLowerCase()}.json`);
            // console.log("----->> attribtes--->", JSON.stringify(preprocessedData))
            let currentQuestion = Object.assign({}, questionData.data.questions[lodash_1.default.random(0, questionData.data.questions.length - 1)]);
            let formattedOptions = [];
            for (let i = 0; i < currentQuestion.options.length; i++) {
                let resultingOption = {
                    "id": String.fromCharCode(65 + i),
                    "option": currentQuestion.options[i]
                };
                formattedOptions.push(resultingOption);
            }
            currentQuestion.options = [];
            currentQuestion.options = formattedOptions;
            let outputSpeech = `${currentQuestion.text} <break time="0.5s"/> option A ${currentQuestion.options[0].option} <break time="0.5s"/> option B ${currentQuestion.options[1].option} <break time="0.5s"/> option C ${currentQuestion.options[2].option} <break time="0.5s"/> option D ${currentQuestion.options[3].option}`;
            yield Promise.all([(0, DbUpdates_1.updateUserLastQuestion)(preprocessedData.userContext.userId, currentQuestion, "quiz", outputSpeech, context)]);
            let timer = (lodash_1.default.has(preprocessedData.requestContext.data.arguments[0], "SLOTS.currentTime")) ? (preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime > 0) ? preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime - 1000 : preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime : questionData.data.timer || 0;
            // console.log("--------40. QuizAction.ts----------", timer)
            let user = yield Promise.all([(0, DbUpdates_1.databaseGet)(preprocessedData.userContext.userId, context)]);
            console.log("---------ISREPEAT-----------", isRepeat);
            console.log("---------------", JSON.parse(JSON.stringify(user[0].last_question)));
            let dataSource = {
                "data": {
                    "categoryColor": questionData.data.categoryColor,
                    "questions": currentQuestion,
                    "backgroundImageURL": questionData.data.backgroundImageURL,
                    "timer": timer,
                    "backgroundVideoURL": "",
                    "category": questionData.data.category,
                    "currentRound": user[0].round,
                    "currentScore": user[0].last_score,
                    "totalScore": user[0].total_score
                }
            };
            let data = {
                "dataSources": dataSource,
                // "outputSpeech": `<speak>${currentQuestion.text} <break time="0.5s"/> option A ${currentQuestion.options[0].option} <break time="0.5s"/> option B ${currentQuestion.options[1].option} <break time="0.5s"/> option C ${currentQuestion.options[2].option} <break time="0.5s"/> option D ${currentQuestion.options[3].option}</speak>`,
                "outputSpeech": `<speak>${outputSpeech}</speak>`,
                "sendEvent": (0, Utils_1.sendEventHelper)(preprocessedData)
            };
            let businessOutput = {
                displayType: 'APL',
                speechType: 'SSML',
                configData: { "templateURL": `https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/quizScreen.json` },
                data: data // dataSource
            };
            return businessOutput;
        });
    }
}
exports.default = QuizAction;

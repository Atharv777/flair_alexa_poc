"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const DbUpdates_1 = require("../Common/DbUpdates");
const Utils_1 = require("../Common/Utils");
class CategoryAction {
    executeAction(preprocessedData, context) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("-------------7. CHOOSE_CATEGORY_ACTION-----------")
            yield Promise.all([(0, DbUpdates_1.updateUserLastQuestion)(preprocessedData.userContext.userId, null, "", "", context)]);
            let rounds_played = yield (0, DbUpdates_1.getRoundsPlayed)(preprocessedData.userContext.userId, context);
            // console.log("-------------ROUNDS_PLAYED---------", rounds_played)
            if (rounds_played < 3) {
                let data = {
                    "dataSources": require('../DataSources/category.json'),
                    "outputSpeech": "<speak>Choose any Category you want to play</speak>",
                    "sendEvent": (0, Utils_1.sendEventHelper)(preprocessedData)
                };
                let businessOutput = {
                    displayType: 'APL',
                    speechType: 'SSML',
                    configData: { "templateURL": "https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/categoryScreen.json" },
                    data: data // dataSource
                };
                return businessOutput;
            }
            else {
                return {};
            }
        });
    }
}
exports.default = CategoryAction;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const DbUpdates_1 = require("../Common/DbUpdates");
const Utils_1 = require("../Common/Utils");
class RepeatAction {
    executeAction(preprocessedData, context) {
        return __awaiter(this, void 0, void 0, function* () {
            let userData = yield Promise.all([(0, DbUpdates_1.databaseGet)(preprocessedData.userContext.userId, context)]);
            let outputSpeech = "";
            if (userData[0].last_context === "quiz") {
                console.log("----------12. RepeatAction.ts---------", JSON.stringify(userData[0]));
                // dataSource = userData[0].last_question
                outputSpeech = "Umm, Sorry I missed that, " + userData[0].last_speech;
            }
            console.log("---------------17. RepeatAction.ts-----------");
            let data = {
                "outputSpeech": `<speak>${outputSpeech}</speak>`,
                "sendEvent": (0, Utils_1.sendEventHelper)(preprocessedData)
            };
            let businessOutput = {
                displayType: '',
                speechType: 'SSML',
                configData: {},
                data: data // dataSource
            };
            return businessOutput;
        });
    }
}
exports.default = RepeatAction;

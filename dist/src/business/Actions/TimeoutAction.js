"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const DbUpdates_1 = require("../Common/DbUpdates");
const CategoryAction_1 = __importDefault(require("./CategoryAction"));
class TimeoutAction {
    executeAction(preprocessedData, context) {
        return __awaiter(this, void 0, void 0, function* () {
            //  console.log("---------------5. TimeoutAction.ts TimeOut occurred-----------")
            yield Promise.all([(0, DbUpdates_1.updateRounds)(preprocessedData.userContext.userId, context)]);
            const response = new CategoryAction_1.default().executeAction(preprocessedData, context);
            return response;
            // let data = {
            //     "dataSources": require('../DataSources/category.json'),
            //     "outputSpeech": "<speak>Timeup!,  Wanna play next round?</speak>",
            //     "sendEvent": sendEventHelper(preprocessedData)
            // }
            // let businessOutput: IActionOutput = {
            //     displayType: 'APL',
            //     speechType: 'SSML',
            //     configData: { "templateURL": "https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/categoryScreen.json" },  // templateURL
            //     data: data // dataSource
            // }
            // return businessOutput
        });
    }
}
exports.default = TimeoutAction;

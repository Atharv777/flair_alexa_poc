"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Utils_1 = require("../Common/Utils");
class LaunchAction {
    executeAction(preprocessedData) {
        return __awaiter(this, void 0, void 0, function* () {
            let data = {
                "dataSources": require('../DataSources/welcome.json'),
                "outputSpeech": "<speak><audio src='https://voicegamesdata.s3.amazonaws.com/tb/prodV2/en-US/sound-files/quiz-welcome.mp3'></audio>Hello, Welcome to Trivia Rush...</speak>",
                "sendEvent": (0, Utils_1.sendEventHelper)(preprocessedData)
            };
            let businessOutput = {
                displayType: 'APL',
                speechType: 'SSML',
                configData: { "templateURL": 'https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/splashScreen.json' },
                data: data // dataSource
            };
            return businessOutput;
        });
    }
}
exports.default = LaunchAction;

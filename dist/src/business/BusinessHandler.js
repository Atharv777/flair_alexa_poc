"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.businessHandler = void 0;
const IntentMap_1 = require("../../IntentMap");
const flair_aws_infra_1 = require("@flairlabs/flair-aws-infra");
let businessHandler = (input) => __awaiter(void 0, void 0, void 0, function* () {
    let actionInput = IntentMap_1.IntentMap[input.verb] ? IntentMap_1.IntentMap[input.verb] : IntentMap_1.IntentMap["UNKNOWN"];
    let context = getContext();
    const response = yield actionInput.executeAction(input, context);
    return response;
});
exports.businessHandler = businessHandler;
function getContext() {
    let context = {
        dao: flair_aws_infra_1.DynamoDao.getDynamoDao(),
        logger: flair_aws_infra_1.Logger.getLogger(),
        rpc: flair_aws_infra_1.RPC.getRPC()
    };
    return context;
}

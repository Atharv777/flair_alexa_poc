"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postProcessingHandler = void 0;
const _ = __importStar(require("lodash"));
const postprocessing_1 = require("./postprocessing");
let postProcessingHandler = (businessOutput) => __awaiter(void 0, void 0, void 0, function* () {
    var directives = [];
    // let outputSpeech = getOutputSpeech(event)
    directives = yield (0, postprocessing_1.getDirectives)(businessOutput);
    let response = {
        shouldEndSession: false,
        outputSpeech: {
            "type": "SSML",
            "ssml": businessOutput.data.outputSpeech
        },
        directives: directives
    };
    if (_.has(businessOutput, "data.dataSources.data.timer")) {
        var sessionAttributes = {
            // timerVal: businessOutput.data.dataSources.data.timer,
            totalScore: 0,
            currentScore: 0,
            currentRound: 0,
            category: businessOutput.data.dataSources.data.category
        };
        let alexaResponse = {
            version: "",
            response: response,
            sessionAttributes: sessionAttributes
        };
        return alexaResponse;
    }
    let alexaResponse = {
        version: "",
        response: response
    };
    // console.log("--------42. postprocessed.ts postprocessed data--------", JSON.stringify(alexaResponse))
    return alexaResponse;
});
exports.postProcessingHandler = postProcessingHandler;

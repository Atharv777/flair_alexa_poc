{
  "type": "APL",
  "version": "2023.2",
  "license": "Copyright 2023 Amazon.com, Inc. or its affiliates. All Rights Reserved.\nSPDX-License-Identifier: LicenseRef-.amazon.com.-AmznSL-1.0\nLicensed under the Amazon Software License  http://aws.amazon.com/asl/",
  "settings": {},
  "theme": "dark",
  "import": [
    {
      "name": "alexa-layouts",
      "version": "1.5.0"
    }
  ],
  "resources": [
    {
      "description": "Hub Landscape Large",
      "dimensions": {
        "questionText": "40px",
        "optionBoxHeight": "100dp",
        "optionBoxWeight": "400dp",
        "optionTextSize": "35px",
        "frameHeight": "40%",
        "touchWrapperWidth": "50%",
        "timerFontSize": "30"
      },
      "number": {
        "ELE_PER_ROW": 2
      }
    },
    {
      "when": "${@viewportProfile == @hubRoundSmall}",
      "description": "Hub Round Small",
      "dimensions": {
        "questionText": "25px",
        "optionBoxHeight": "80dp",
        "optionBoxWeight": "300dp",
        "optionTextSize": "35px",
        "frameHeight": "50%",
        "touchWrapperWidth": "70%",
        "timerFontSize": "25"
      },
      "number": {
        "ELE_PER_ROW": 1
      }
    },
    {
      "when": "${@viewportSizeClass == @viewportClassMediumSmall}",
      "description": "Hub Landscape Medium",
      "dimensions": {
        "questionText": "30dp",
        "optionBoxHeight": "80dp",
        "optionBoxWeight": "280dp",
        "optionTextSize": "30px",
        "frameHeight": "40%",
        "touchWrapperWidth": "50%",
        "timerFontSize": "23"
      },
      "number": {
        "ELE_PER_ROW": 2
      }
    },
    {
      "when": "${@viewportSizeClass == @viewportClassMediumXSmall}",
      "description": "Hub Landscape small",
      "dimensions": {
        "questionText": "25dp",
        "optionBoxHeight": "70dp",
        "optionBoxWeight": "250dp",
        "optionTextSize": "30px",
        "frameHeight": "40%",
        "touchWrapperWidth": "50%",
        "timerFontSize": "30"
      },
      "number": {
        "ELE_PER_ROW": 2
      }
    },
    {
      "when": "${@viewportSizeClass == @viewportClassXLargeMedium}",
      "description": "Hub Landscape Extra large",
      "dimensions": {
        "questionText": "60px",
        "optionBoxHeight": "120dp",
        "optionBoxWeight": "550dp",
        "optionTextSize": "50px",
        "frameHeight": "40%",
        "touchWrapperWidth": "50%",
        "timerFontSize": "60"
      },
      "number": {
        "ELE_PER_ROW": 2
      }
    },
    {
      "when": "${@viewportSizeClass == @viewportClassMediumXLarge}",
      "description": "Hub portrait Medium",
      "dimensions": {
        "questionText": "60px",
        "optionBoxHeight": "140dp",
        "optionBoxWeight": "600dp",
        "optionTextSize": "60px",
        "progressBarWidth": "70%",
        "frameHeight": "20%",
        "touchWrapperWidth": "80%",
        "timerFontSize": "50"
      },
      "number": {
        "ELE_PER_ROW": 1
      }
    },
    {
      "when": "${@viewportSizeClass == @viewportClassSmallMedium}",
      "description": "Mobile Small",
      "dimensions": {
        "questionText": "40px",
        "optionBoxHeight": "100dp",
        "optionBoxWeight": "400dp",
        "optionTextSize": "35px",
        "progressBarWidth": "70%",
        "frameHeight": "50%",
        "touchWrapperWidth": "80%",
        "timerFontSize": "40"
      },
      "number": {
        "ELE_PER_ROW": 1
      }
    },
    {
      "when": "${@viewportProfile == @tvLandscapeXLarge}",
      "description": "TV",
      "dimensions": {
        "optionTextSize": "45px"
      }
    }
  ],
  "styles": {},
  "onMount": [],
  "graphics": {},
  "commands": [],
  "layouts": {},
  "mainTemplate": {
    "parameters": ["payload"],
    "items": [
      {
        "justifyContent": "spaceEvenly",
        "alignItems": "center",
        "bind": [
          {
            "name": "progressTime",
            "value": "${payload.data.timer}"
          }
        ],
        "type": "Container",
        "id": "questionWindow",
        "height": "100%",
        "width": "100%",
        "items": [
          {
            "type": "AlexaBackground",
            "opacity": "0.5",
            "backgroundVideoSource": [
              {
                "url": "${payload.data.backgroundVideoURL}",
                "repeatCount": 2
              }
            ],
            "videoAutoPlay": true,
            "videoAudioTrack": "none"
          },
          {
            "alignItems": "start",
            "justifyContent": "center",
            "direction": "row",
            "type": "Container",
            "id": "outerContainer",
            "height": "25%",
            "width": "100%",
            "items": [
              {
                "alignItems": "center",
                "justifyContent": "center",
                "type": "Container",
                "height": "100%",
                "width": "50%",
                "items": [
                  {
                    "type": "AlexaProgressBar",
                    "progressValue": "${progressTime}",
                    "bufferValue": "60000",
                    "totalValue": 60000,
                    "isLoading": true,
                    "spacing": "@spacingXLarge",
                    "progressFillColor": "#86f059",
                    "handleTick": [
                      {
                        "when": "${progressValue > 0}",
                        "minimumDelay": 1000,
                        "commands": [
                          {
                            "type": "SetValue",
                            "property": "progressTime",
                            "value": "${progressValue - 1000}"
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                "text": "${progressTime / 1000}",
                "textAlign": "center",
                "textAlignVertical": "center",
                "fontSize": "@timerFontSize",
                "type": "Text",
                "height": "100%",
                "width": "10%",
                "onMount": {
                  "type": "SendEvent",
                  "delay": "${payload.data.timer}",
                  "arguments": [
                    {
                      "EventName": "TIMEOUT_INTENT"
                    }
                  ]
                }
              }
            ]
          },
          {
            "text": "${payload.data.questions.text}",
            "id": "questionId",
            "textAlign": "center",
            "color": "${payload.data.categoryColor}",
            "textAlignVertical": "center",
            "fontWeight": "bold",
            "fontSize": "@questionText",
            "type": "Text",
            "height": "20%",
            "width": "80%"
          },
          {
            "data": "${Array.range(0,payload.data.questions.options.length,@ELE_PER_ROW)}",
            "type": "Sequence",
            "height": "60%",
            "width": "100%",
            "items": [
              {
                "type": "Container",
                "data": "${Array.slice(payload.data.questions.options,data,data+@ELE_PER_ROW)}",
                "direction": "${@viewportOrientation == @viewportOrientationPortrait || @viewportProfile == @hubRoundSmall ? 'column' : 'row'}",
                "alignItems": "center",
                "width": "100%",
                "height": "@frameHeight",
                "items": [
                  {
                    "onPress": [
                      {
                        "type": "SendEvent",
                        "arguments": [
                          {
                            "EventName": "ANSWER_INTENT",
                            "SLOTS": {
                              "SLOT_TYPE": "answer",
                              "SLOT_VALUE": "${data.option}",
                              "SLOT_NAME": "${data.option}",
                              "SLOT_ID": "",
                              "currentTime": "${progressTime}",
                              "category": "${payload.data.category}"
                            }
                          }
                        ]
                      },
                      {
                        "type": "SetValue",
                        "property": "backgroundColor",
                        "componentId": "${data.id}",
                        "value": "${data.option == payload.data.questions.answer ? 'green' : 'red'}"
                      }
                    ],
                    "type": "TouchWrapper",
                    "height": "100%",
                    "width": "@touchWrapperWidth",
                    "items": [
                      {
                        "alignItems": "center",
                        "justifyContent": "center",
                        "type": "Container",
                        "height": "100%",
                        "width": "100%",
                        "items": [
                          {
                            "backgroundColor": "yellow",
                            "borderRadius": "20",
                            "id": "${data.id}",
                            "type": "Frame",
                            "height": "@optionBoxHeight",
                            "width": "@optionBoxWeight",
                            "items": [
                              {
                                "color": "black",
                                "text": "${data.option}",
                                "fontSize": "@optionTextSize",
                                "letterSpacing": 2,
                                "textAlignVertical": "center",
                                "fontFamily": "san serif",
                                "textAlign": "center",
                                "fontWeight": "bold",
                                "layoutDirection": "RTL",
                                "type": "Text",
                                "height": "100%",
                                "width": "100%"
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
}

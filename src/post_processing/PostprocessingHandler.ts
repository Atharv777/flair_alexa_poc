import { AlexaResponse, Response, SessionAttributes } from "./IPostOutput"
import * as fs from 'fs'
import * as _ from 'lodash'
import { getDirectives } from "./postprocessing"
import { IActionOutput } from "../business/IActionOutput"

export let postProcessingHandler = async (businessOutput: IActionOutput) => {

    var directives: any[] = []

    // let outputSpeech = getOutputSpeech(event)
    directives = await getDirectives(businessOutput)

    let response: Response = {
        shouldEndSession: false,
        outputSpeech: {
            "type": "SSML",
            "ssml": businessOutput.data.outputSpeech
        },
        directives: directives
    }

    if(_.has(businessOutput, "data.dataSources.data.timer")){
        
        var sessionAttributes:SessionAttributes = {
            // timerVal: businessOutput.data.dataSources.data.timer,
            totalScore: 0,
            currentScore: 0,
            currentRound: 0,
            category:businessOutput.data.dataSources.data.category
        }

        let alexaResponse: AlexaResponse = {
            version: "",
            response: response,
            sessionAttributes:sessionAttributes
        }

        return alexaResponse
    }

    let alexaResponse: AlexaResponse = {
        version: "",
        response: response
    }

    // console.log("--------42. postprocessed.ts postprocessed data--------", JSON.stringify(alexaResponse))
    return alexaResponse
}
export interface AlexaResponse {
    version: string;
    response: Response;
    sessionAttributes?: SessionAttributes;
}

export interface Response {
    shouldEndSession: boolean;
    outputSpeech: OutputSpeech;
    type?: string;
    reprompt?: Reprompt;
    directives?: Object[];
}
export interface OutputSpeech {
    type: string;
    ssml?: string;
    text?: string;
}

export interface Reprompt {
    outputSpeech: OutputSpeech;
}

// export interface SessionAttributes {
//     PLATFORM: string;
//     IS_NEWS_SESSION: boolean;
//     LOCALE: string;
//     STAGE: string;
//     APPNAME: string;
//     DEVICE_CONFIG: DeviceConfig;
//     SESSION_ID: string;
//     IS_MONETIZATION_ENABLED: boolean;
//     LOCALE_CONTENT: string;
//     timerVal:number;
// }

export interface SessionAttributes {
    totalScore:number,
    currentScore:number,
    currentRound:number,
    category:string,
}

export interface DeviceConfig {
    DEVICE_TIMEZONE: string;
    DEVICE_ID: string;
    DEVICE_TYPE: string;
    IS_DISPLAY_ENABLED: boolean;
    HEIGHT: number;
    WIDTH: number;
    DPI: number;
    APL_MAX_VERSION: string;
    CAN_ROTATE: boolean;
    LAST_APL_TOKEN: string;
}
import axios from "axios";


export async function getTemplate(templateURL: any): Promise<any> {

    try{
        let data  = (await axios.get(templateURL)).data
        // console.log("--------------AXIOS-----------", JSON.stringify(data))
        return data
    } catch(error){
        console.log("Error while fetching", error)
    }

}
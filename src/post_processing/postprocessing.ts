import _ from "lodash";
import { getTemplate } from "./handler";

export async function getDirectives(businessOutput: any): Promise<Object[]> {

    if (_.has(businessOutput.configData, "templateURL")) {
        let document = await getTemplate(businessOutput.configData.templateURL)
        return [
            {
                type: "Alexa.Presentation.APL.RenderDocument",
                token: "image-no-text",
                version: "1.5",
                document: document,
                datasources: businessOutput.data.dataSources,
            },
            {
                type: "Alexa.Presentation.APL.ExecuteCommands",
                token: "image-no-text",
                commands: [{
                    type: "Sequential",
                    commands: [businessOutput.data.sendEvent]
                }]
            }
        ]
    }

    return [
        {
            type: "Alexa.Presentation.APL.RenderDocument",
            token: "image-no-text",
            version: "1.5"
        }
    ]

}
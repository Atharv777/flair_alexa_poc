import _ from "lodash"
import { IUserContext, ISessionContext, IRequestContext, IActionInput, IDeviceContext, AlexaInterfaces } from "./IActionInput"

// enum DefaultActions {
//     // LEADERBOARD_FOR_STATE = "LEADERBOARD_FOR_STATE",
//     // NATION_LEADERBOARD = "NATION_LEADERBOARD",
//     CONNETION_RESPONSE = "CONNETION_RESPONSE",
//     BUY_INTENT = "BUY_INTENT",
//     REFUND_INTENT = "REFUND_INTENT",
//     SESSION_END = "SESSION_END",
//     UNKNOWN = "UNKNOWN",
//     END = "END",
//     HELP = "HELP",
//     // CONTINUE_GAME = "CONTINUE_GAME",
//     // PLAYER_STATISTICS = "PLAYER_STATISTICS",
//     LINK_ACCOUNT = "LINK_ACCOUNT",
//     SESSION_RESUMED_REQUEST = "SESSION_RESUMED_REQUEST",
//     CUSTOM_TASK = "CUSTOM_TASK",
//     WHAT_DID_I_BUY = "WHAT_DID_I_BUY",
//     WHAT_CAN_I_BUY = "WHAT_CAN_I_BUY",

//     LAUNCH = "LAUNCH",
//     YES = "YES",
//     NO = "NO",
//     REPEAT = "REPEAT",
//     // LEADERBOARD = "LEADERBOARD",
//     // ANSWER_INTENT = "ANSWER_INTENT",
//     // ASK_QUESTION = "ASK_QUESTION",
//     // USER_STATE_INTENT = "USER_STATE_INTENT",
//     NEXT = "NEXT",
//     USER_EVENT = "USER_EVENT",
//     // BC_HOME = "BC_HOME",
//     // AOT_HOME = "AOT_HOME",
//     // CM_HOME = "CM_HOME",
//     // MHA_HOME = "MHA_HOME"
// }

// let IntentMap: { [key: string]: DefaultActions } = {
//     "LaunchRequest": DefaultActions.LAUNCH,
//     // "LeaderboardIntent": DefaultActions.LEADERBOARD,
//     // "UserstateIntent": DefaultActions.USER_STATE_INTENT,
//     // "AnswerIntent": DefaultActions.ANSWER_INTENT,
//     // "QuestionIntent": DefaultActions.ASK_QUESTION,
//     "RepeatIntent": DefaultActions.REPEAT,
//     "AMAZON.YesIntent": DefaultActions.YES,
//     "AMAZON.NoIntent": DefaultActions.NO,
//     "AMAZON.NextIntent": DefaultActions.NEXT,
//     // "BlackCloverIntent": DefaultActions.BC_HOME,
//     // "AttackOnTitanIntent": DefaultActions.AOT_HOME,
//     // "ChainsawmanIntent": DefaultActions.CM_HOME,
//     // "MHAIntent": DefaultActions.MHA_HOME,
// }

enum DefaultActions {
    CONNETION_RESPONSE = "CONNETION_RESPONSE",
    BUY_INTENT = "BUY_INTENT",
    REFUND_INTENT = "REFUND_INTENT",
    SESSION_END = "SESSION_END",
    UNKNOWN = "UNKNOWN",
    END = "END",
    HELP = "HELP",
    LINK_ACCOUNT = "LINK_ACCOUNT",
    SESSION_RESUMED_REQUEST = "SESSION_RESUMED_REQUEST",
    CUSTOM_TASK = "CUSTOM_TASK",
    WHAT_DID_I_BUY = "WHAT_DID_I_BUY",
    WHAT_CAN_I_BUY = "WHAT_CAN_I_BUY",
    LAUNCH = "LAUNCH",
    YES = "YES_INTENT",
    NO = "NO",
    REPEAT = "REPEAT",
    NEXT = "NEXT",
    USER_EVENT = "USER_EVENT"
}

let defaultIntentMap: { [key: string]: DefaultActions } = {
    "LaunchRequest": DefaultActions.LAUNCH,
    "RepeatIntent": DefaultActions.REPEAT,
    "AMAZON.YesIntent": DefaultActions.YES,
    "AMAZON.NoIntent": DefaultActions.NO,
    "AMAZON.NextIntent": DefaultActions.NEXT,
    "AMAZON.CONNETION_RESPONSE": DefaultActions.CONNETION_RESPONSE,
    "AMAZON.BUY_INTENT": DefaultActions.BUY_INTENT,
    "AMAZON.REFUND_INTENT": DefaultActions.REFUND_INTENT,
    "AMAZON.SESSION_END": DefaultActions.SESSION_END,
    "AMAZON.UNKNOWN": DefaultActions.UNKNOWN,
    "AMAZON.END": DefaultActions.END,
    "AMAZON.HELP": DefaultActions.HELP,
    "AMAZON.LINK_ACCOUNT": DefaultActions.LINK_ACCOUNT,
    "AMAZON.SESSION_RESUMED_REQUEST": DefaultActions.SESSION_RESUMED_REQUEST,
    "AMAZON.CUSTOM_TASK": DefaultActions.CUSTOM_TASK,
    "AMAZON.WHAT_DID_I_BUY": DefaultActions.WHAT_DID_I_BUY,
    "AMAZON.WHAT_CAN_I_BUY": DefaultActions.WHAT_CAN_I_BUY,
    "Alexa.Presentation.APL.UserEvent": DefaultActions.USER_EVENT,
    "IntentRequest": DefaultActions.USER_EVENT
}

export let preprocessingHandler = async (event: any): Promise<IActionInput> => {

    const userContext: IUserContext = getUserContext(event)
    const sessionContext: ISessionContext = getSessionContext(event)
    const deviceContext: IDeviceContext = getDeviceContext(event)
    const requestContext: IRequestContext = getRequestContext(event)

    userContext.inSkillProducts = await getInskillProducts()

    let { verb, requestData } = parseRequestData(event)
    requestContext.data = requestData


    let actionInput: IActionInput = {
        verb: verb,
        platForm: "Alexa",
        userContext: userContext,
        sessionContext: sessionContext,
        deviceContext: deviceContext,
        requestContext: requestContext
    }

    // console.log("--------(119)Preprocessed data->", JSON.stringify(actionInput))
    return actionInput
}

async function getInskillProducts(): Promise<any[]> { return [] }

function parseRequestData(event: any): { verb: string, requestData: { [key: string]: any } } {

    let requestData: { [key: string]: any } = {}
    let verb = getVerb(event)
    _.set(requestData, "arguments", _.get(event, "request.arguments") || [])
    _.set(requestData, "slots", parseSlotValues(_.get(event, "request.intent.slots") || _.get(event, "request.arguments[0]['SLOTS']") || {}))
    return { verb, requestData }
}

function getVerb(event: any): string {
    let requestType = _.get(event, "request.type")
    let verb = defaultIntentMap[requestType] || _.get(event, "request.intent.name") || DefaultActions.UNKNOWN

    if (verb === 'USER_EVENT') {
        if (_.has(event.request, "arguments[0]['args']['EventName']")) {
            console.log("----------IntentName is inside event.request.arguments[0].args.EventName---------", event.request.arguments[0].args.EventName)
            verb = event.request.arguments[0].args.EventName
        }
        else if (_.has(event.request, "intent")) {
            console.log("--------IntentName is inside event.request.intent.name------------", event.request.intent.name)
            verb = event.request.intent.name === "CHOOSE_CATEGORY_INTENT" ? "QUIZ_INTENT" : event.request.intent.name
        }
        else if (_.has(event.request, "arguments[0]['EventName']")) {
            console.log("---------IntentName is inside event.request.arguments[0]['EventName']----------", event.request.arguments[0]['EventName'])
            verb = event.request.arguments[0]['EventName']
        }
    }
    return verb
}

function getUserContext(event: any): IUserContext {
    return {
        userId: _.get(event, "session.user.userId"),
        inSkillProducts: [],
        userAttributes: _.get(event, "userAttributes") || {}
    }
}

function getSessionContext(event: any): ISessionContext {
    return {
        sessionId: _.get(event, "session.sessionId"),
        sessionAttributes: _.get(event, "session.attributes"),
        apiAccessToken: _.get(event, "context.System.apiAccessToken"),
        apiEndpoint: _.get(event, "context.System.apiEndpoint"),
        skillId: _.get(event, "context.System.application.applicationId"),
        isNewSession: _.get(event, "session.new")
    }
}

function getDeviceContext(event: any): IDeviceContext {

    let interfaceReferenceMap: { [key: string]: AlexaInterfaces } = {
        "AudioPlayer": AlexaInterfaces.AUDIO_PLAYER,
        "Alexa.Presentation.APL": AlexaInterfaces.APL
    }

    let supportedInterfaces: string[] = _.keys(_.get(event, "context.System.device.supportedInterfaces") || {})
        .map((e) => { return interfaceReferenceMap[e] || null })
        .filter((e) => e != null)

    return {
        deviceId: _.get(event, "context.System.device.deviceId"),
        shape: _.get(event, "context.Viewport.shape"),
        mode: _.get(event, "context.Viewport.mode"),
        dpi: _.get(event, "context.Viewport.dpi"),
        width: _.get(event, "context.Viewport.currentPixelWidth"),
        height: _.get(event, "context.Viewport.currentPixelHeight"),
        isDisplayEnabled: supportedInterfaces.includes(AlexaInterfaces.APL),
        supportedInterfaces
    }
}

function getRequestContext(event: any): IRequestContext {
    return {
        requestId: _.get(event, "request.requestId"),
        timestamp: _.get(event, "request.timestamp"),
        locale: _.get(event, "request.locale"),
        data: {}
    }
}


function parseSlotValues(slotData: any): { slotValue: string, slotId: string, slotName: string }[] {
    let slots: { slotValue: string, slotId: string, slotName: string, test: string }[] = []

    let slotObjKeys = Object.keys(slotData)
    if (slotObjKeys.includes("category") || slotObjKeys.includes("boolean") || slotObjKeys.includes("answer_option") || slotObjKeys.includes("answer_value") || slotObjKeys.includes("user_stat")) {
        console.log("---------212. preprocessing.ts slotData----------", slotData)
        _.keys(slotData || {}).forEach((e) => {
            let element = slotData[e] || {}
            if (_.has(element, "resolutions")) {
                slots.push({
                    slotValue: _.get(element, "value") || _.get(element, "resolutions.resolutionsPerAuthority[0].values[0].value.name"),
                    slotId: _.get(element, "resolutions.resolutionsPerAuthority[0].values[0].value.id"),
                    slotName: _.get(element, "name") || e,
                    test: "Speech Event"
                })
            }
        })
        return slots
    }
    else {
        console.log("---------226. preprocessing.ts slotData----------", slotData)
        slots.push(
            {
                slotValue: slotData['SLOT_VALUE'],
                slotId: slotData['SLOT_ID'],
                slotName: slotData['SLOT_NAME'],
                test: "Touch Event"
            }
        )
        return slots
    }
}
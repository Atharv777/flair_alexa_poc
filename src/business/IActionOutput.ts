export interface IActionOutput {
    displayType: string // APL - AUDIO_PLAYER - MOBILE
    speechType: string // SPEECH - APLA
    configData: { [key: string]: any } // templateUrl dataSource = [""]
    data: { [key: string]: any } // welcomeText logoImage listDataL []
    shouldEndSession?: boolean
}

// export interface BusinessResponse {
//     ACTION: Action;
//     OBJS:   Objs;
//     DATA:   Data;
// }

// export interface Action {
//     prompt:            boolean;
//     reprompt:          boolean;
//     shouldEndSession:  boolean;
//     sessionAttributes: boolean;
// }

// export interface Data {
//     promptSpeech:   string;
//     repromptSpeech: string;
// }

// export interface Objs {
//     sessionAttributes: SessionAttributes;
// }

// export interface SessionAttributes {
//     PLATFORM:                string;
//     IS_NEWS_SESSION:         boolean;
//     LOCALE:                  string;
//     STAGE:                   string;
//     APPNAME:                 string;
//     DEVICE_CONFIG:           DeviceConfig;
//     SESSION_ID:              string;
//     IS_MONETIZATION_ENABLED: boolean;
//     LOCALE_CONTENT:          string;
// }

// export interface DeviceConfig {
//     DEVICE_TIMEZONE:    string;
//     DEVICE_ID:          string;
//     DEVICE_TYPE:        string;
//     IS_DISPLAY_ENABLED: boolean;
//     HEIGHT:             number;
//     WIDTH:              number;
//     DPI:                number;
//     APL_MAX_VERSION:    string;
//     CAN_ROTATE:         boolean;
//     LAST_APL_TOKEN:     string;
// }

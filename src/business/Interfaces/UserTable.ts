export interface UserTableStructure {
    user_id: string,
    last_question: LastQuestion,
    total_score: number,
    last_score: number,
    round: number,
    play_date: any,
    rounds_played: number,
    last_context:string,
    last_speech:string
}

interface LastQuestion{
    answer : string,
    options:any,
    text:string
}
import { UserTableStructure } from "../Interfaces/UserTable"
import { formatDate } from "./Utils"

export function databaseGet(user_id: any, context: any): Promise<any> {
    return new Promise((resolve, reject) => {
        context.dao.getById("trivia-rush-user-data", { user_id: user_id }, (data: any) => {
            resolve(data)
        })
    })
}
export function updateUserData(user_id: string, total_score: number, last_score: number, round: number, rounds_played: number, context: any): Promise<any> {
    let data = {
        user_id: user_id,
        last_question: null,
        total_score: total_score,
        last_score: last_score,
        round: round,
        play_date: formatDate(new Date()),
        rounds_played: rounds_played
    }
    // console.log("--------update user data---------", data)
    return new Promise((resolve, reject) => {
        context.dao.save("trivia-rush-user-data", data, (data: boolean) => {
            resolve(data)
        })
    })
}

export async function getRoundsPlayed(user_id: string, context: any) {
    let user = await Promise.all([databaseGet(user_id, context)])
    if (user[0]) {
        return user[0].rounds_played
    }
}

export async function updateRounds(user_id: string, context: any) {
    let user = await Promise.all([databaseGet(user_id, context)])
    if (user[0].play_date === formatDate(new Date()) && user[0].round <= 3) {
        let data = {
            user_id: user_id,
            round: user[0].round + 1,
            rounds_played: user[0].round,
            play_date: formatDate(new Date()),
            last_question: null,
            total_score: user[0].total_score,
            last_score: 0
        }
        // console.log("--------update rounds---------", data)
        return new Promise((resolve, reject) => {
            context.dao.save("trivia-rush-user-data", data, (data: boolean) => {
                resolve(data)
            })
        })
    }
}

export async function updateUserLastQuestion(user_id: string, questionSet: any, last_context:string, last_speech:string, context: any) {
    let user = await Promise.all([databaseGet(user_id, context)])
    let data: UserTableStructure = {
        user_id: "",
        last_question: {
            answer: "",
            options: undefined,
            text: ""
        },
        total_score: 0,
        last_score: 0,
        round: 0,
        play_date: undefined,
        rounds_played: 0,
        last_context: "",
        last_speech: ""
    }
    if (user[0]) {
        if (user[0].play_date !== formatDate(new Date())) {
            data = {
                user_id: user_id,
                last_question: questionSet,
                total_score: user[0].total_score,
                last_score: 0,
                round: 1,
                play_date: formatDate(new Date()),
                rounds_played: user[0].rounds_played,
                last_context: last_context,
                last_speech: last_speech
            }
            // console.log("--------different date---------", data)
        }
        else {
            data = {
                user_id: user_id,
                last_question: questionSet,
                total_score: user[0].total_score,
                last_score: user[0].last_score,
                round: user[0].round,
                play_date: formatDate(new Date()),
                rounds_played: user[0].rounds_played,
                last_context: last_context,
                last_speech: last_speech
            }
            // console.log("--------same date---------", data)
        }
    }
    else {
        data = {
            user_id: user_id,
            last_question: questionSet,
            total_score: 0,
            last_score: 0,
            round: 1,
            play_date: formatDate(new Date()),
            rounds_played: 0,
            last_context: last_context,
            last_speech: last_speech
        }
        // console.log("--------new user---------", data)
    }

    return new Promise((resolve, reject) => {
        context.dao.save("trivia-rush-user-data", data, (data: boolean) => {
            resolve(data)
        })
    })
}
export function sendEventHelper(preprocessedData:any){
    if(preprocessedData.verb === "LAUNCH"){
        let sendEventObj = {
            type:"SendEvent",
            token:"image-no-text",
            arguments:[
                {
                    args:{
                        EventName:"CHOOSE_CATEGORY_INTENT"
                    }
                }
            ]
        }
        return sendEventObj
    }

    return {}
}

export function padTo2Digits(num:number) {
    return num.toString().padStart(2, '0');
  }
  
export function formatDate(date:any) {
    return [
      date.getFullYear(),
      padTo2Digits(date.getMonth() + 1),
      padTo2Digits(date.getDate()),
    ].join('-');
  }
import * as _ from 'lodash'
import { IActionOutput } from './IActionOutput'
import { IntentMap } from '../../IntentMap'
import { IActionInput } from '../IActionInput'
import { DynamoDao, Logger, RPC } from '@flairlabs/flair-aws-infra'
import { IContext } from '@flairlabs/flair-infra'


export let businessHandler = async (input: IActionInput): Promise<IActionOutput> => {

    let actionInput = IntentMap[input.verb] ? IntentMap[input.verb] : IntentMap["UNKNOWN"]
    let context = getContext()
    const response: IActionOutput = await actionInput.executeAction(input, context)

    return response

}

function getContext() {
    let context = {
        dao: DynamoDao.getDynamoDao(),
        logger: Logger.getLogger(),
        rpc: RPC.getRPC()
    }
    return context
}
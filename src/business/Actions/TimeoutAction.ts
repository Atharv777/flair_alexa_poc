import { updateRounds } from "../Common/DbUpdates"
import { sendEventHelper } from "../Common/Utils"
import { IActionOutput } from "../IActionOutput"
import CategoryAction from "./CategoryAction"

export default class TimeoutAction {
    async executeAction(preprocessedData: any, context:any) {
        //  console.log("---------------5. TimeoutAction.ts TimeOut occurred-----------")

        await Promise.all([updateRounds(preprocessedData.userContext.userId, context)])
        const response = new CategoryAction().executeAction(preprocessedData, context)

        return response

        // let data = {
        //     "dataSources": require('../DataSources/category.json'),
        //     "outputSpeech": "<speak>Timeup!,  Wanna play next round?</speak>",
        //     "sendEvent": sendEventHelper(preprocessedData)
        // }
        // let businessOutput: IActionOutput = {
        //     displayType: 'APL',
        //     speechType: 'SSML',
        //     configData: { "templateURL": "https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/categoryScreen.json" },  // templateURL
        //     data: data // dataSource
        // }

        // return businessOutput
    }
}
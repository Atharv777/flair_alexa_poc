import { getRoundsPlayed, updateUserLastQuestion } from "../Common/DbUpdates"
import { sendEventHelper } from "../Common/Utils"
import { IActionOutput } from "../IActionOutput"

export default class CategoryAction{
     async executeAction(preprocessedData: any,context:any) {
        // console.log("-------------7. CHOOSE_CATEGORY_ACTION-----------")

        await Promise.all([updateUserLastQuestion(preprocessedData.userContext.userId, null, "", "",context)])

        let rounds_played = await getRoundsPlayed(preprocessedData.userContext.userId,context)
        
        // console.log("-------------ROUNDS_PLAYED---------", rounds_played)

        if (rounds_played < 3) {
            let data = {
                "dataSources": require('../DataSources/category.json'),
                "outputSpeech": "<speak>Choose any Category you want to play</speak>",
                "sendEvent": sendEventHelper(preprocessedData)
            }
            let businessOutput: IActionOutput = {
                displayType: 'APL',
                speechType: 'SSML',
                configData: { "templateURL": "https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/categoryScreen.json" },  // templateURL
                data: data // dataSource
            }
            return businessOutput
        }
        else{
            return {}
        }
      }
 }
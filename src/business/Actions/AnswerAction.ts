import _ from "lodash"
import QuizAction from "./QuizAction"
import { databaseGet, updateUserData } from "../Common/DbUpdates"
import RepeatAction from "./RepeatAction"

export default class AnswerAction {
    async executeAction(preprocessedData: any, context: any) {

        let slotValue = preprocessedData.requestContext.data.slots[0].slotValue
        console.log("-----------9. AnswerAction.ts slotValue -------------", slotValue)
        let user = await Promise.all([databaseGet(preprocessedData.userContext.userId, context)])
        // console.log("-----------12. AnswerAction.ts userData--------", user)
        if (user[0]) {
            let questionSet = user[0].last_question
            let totalScore = user[0].total_score
            let lastScore = user[0].last_score
            if (slotValue.toLowerCase() === questionSet.answer.toLowerCase()) {
                if (lastScore === 50) {
                    totalScore = totalScore + 100
                    lastScore = 100
                }
                else if (lastScore === 100 || lastScore === 150) {
                    totalScore = totalScore + 150
                    lastScore = 150
                }
                else {
                    totalScore = totalScore + 50
                    lastScore = 50
                }
            }
            else {
                totalScore = totalScore
                lastScore = 0
            }
            await Promise.all([updateUserData(user[0].user_id, totalScore, lastScore, user[0].round, user[0].rounds_played, context)])
        } else {
            await Promise.all([updateUserData(preprocessedData.userContext.userId, 50, 110, 1, 0, context)])
        }

        const response = new QuizAction().executeAction(preprocessedData, context)

        return response

    }
}

import { databaseGet } from "../Common/DbUpdates"
import { sendEventHelper } from "../Common/Utils"
import { IActionOutput } from "../IActionOutput"

export default class RepeatAction {
    async executeAction(preprocessedData: any, context: any) {

        let userData = await Promise.all([databaseGet(preprocessedData.userContext.userId, context)])
        let outputSpeech:string = ""
        if(userData[0].last_context === "quiz"){
            console.log("----------12. RepeatAction.ts---------", JSON.stringify(userData[0]))
            // dataSource = userData[0].last_question
            outputSpeech = "Umm, Sorry I missed that, " + userData[0].last_speech
        }
        
        console.log("---------------17. RepeatAction.ts-----------")
        let data = {
            "outputSpeech": `<speak>${outputSpeech}</speak>`,
            "sendEvent": sendEventHelper(preprocessedData)
        }
        let businessOutput: IActionOutput = {
            displayType: '',
            speechType: 'SSML',
            configData: {},  // templateURL
            data: data // dataSource
        }

        return businessOutput

    }
}
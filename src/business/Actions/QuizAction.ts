import _ from "lodash"
import { sendEventHelper } from "../Common/Utils"
import { IActionOutput } from "../IActionOutput"
import { databaseGet, updateUserLastQuestion } from "../Common/DbUpdates"

export default class QuizAction {
    async executeAction(preprocessedData: any,context: any, isRepeat?:boolean) {
        let questionData: any = {}
        questionData = (_.has(preprocessedData.sessionContext.sessionAttributes, "category")) ? require(`../DataSources/${preprocessedData.sessionContext.sessionAttributes.category}.json`) : require(`../DataSources/${preprocessedData.requestContext.data.slots[0].slotValue.toLowerCase()}.json`)
        // console.log("----->> attribtes--->", JSON.stringify(preprocessedData))

        let currentQuestion = { ...questionData.data.questions[_.random(0, questionData.data.questions.length - 1)] }
        let formattedOptions: any = []
        for (let i = 0; i < currentQuestion.options.length; i++) {
            let resultingOption = {
                "id": String.fromCharCode(65 + i),
                "option": currentQuestion.options[i]
            }
            formattedOptions.push(resultingOption)
        }

        currentQuestion.options = []
        currentQuestion.options = formattedOptions
        let outputSpeech = `${currentQuestion.text} <break time="0.5s"/> option A ${currentQuestion.options[0].option} <break time="0.5s"/> option B ${currentQuestion.options[1].option} <break time="0.5s"/> option C ${currentQuestion.options[2].option} <break time="0.5s"/> option D ${currentQuestion.options[3].option}`
        await Promise.all([updateUserLastQuestion(preprocessedData.userContext.userId, currentQuestion, "quiz", outputSpeech, context)])
        let timer = (_.has(preprocessedData.requestContext.data.arguments[0], "SLOTS.currentTime")) ? (preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime > 0) ? preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime - 1000 : preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime : questionData.data.timer || 0
        // console.log("--------40. QuizAction.ts----------", timer)
        let user = await Promise.all([databaseGet(preprocessedData.userContext.userId, context)])
        console.log("---------ISREPEAT-----------", isRepeat)
        console.log("---------------",JSON.parse(JSON.stringify(user[0].last_question)))
        let dataSource = {
            "data": {
                "categoryColor": questionData.data.categoryColor,
                "questions": currentQuestion,
                "backgroundImageURL": questionData.data.backgroundImageURL,
                "timer": timer,
                "backgroundVideoURL": "",
                "category": questionData.data.category,
                "currentRound": user[0].round,
                "currentScore": user[0].last_score,
                "totalScore": user[0].total_score
            }
        }
        
        let data = {
            "dataSources": dataSource,
            // "outputSpeech": `<speak>${currentQuestion.text} <break time="0.5s"/> option A ${currentQuestion.options[0].option} <break time="0.5s"/> option B ${currentQuestion.options[1].option} <break time="0.5s"/> option C ${currentQuestion.options[2].option} <break time="0.5s"/> option D ${currentQuestion.options[3].option}</speak>`,
            "outputSpeech" : `<speak>${outputSpeech}</speak>`,
            "sendEvent": sendEventHelper(preprocessedData)
        }
        let businessOutput: IActionOutput = {
            displayType: 'APL',
            speechType: 'SSML',
            configData: { "templateURL": `https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/quizScreen.json` },  // templateURL
            data: data // dataSource
        }
        return businessOutput
    }
}
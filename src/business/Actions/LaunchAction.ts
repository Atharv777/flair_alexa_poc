import { sendEventHelper } from "../Common/Utils"
import { IActionOutput } from "../IActionOutput"

export default class LaunchAction {
    async executeAction(preprocessedData: any) {

        let data = {
            "dataSources": require('../DataSources/welcome.json'),
            "outputSpeech": "<speak><audio src='https://voicegamesdata.s3.amazonaws.com/tb/prodV2/en-US/sound-files/quiz-welcome.mp3'></audio>Hello, Welcome to Trivia Rush...</speak>",
            "sendEvent": sendEventHelper(preprocessedData)
        }
        let businessOutput: IActionOutput = {
            displayType: 'APL',
            speechType: 'SSML',
            configData: { "templateURL": 'https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/splashScreen.json' },  // templateURL
            data: data // dataSource
        }

        return businessOutput
    }
}
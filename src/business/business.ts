import * as _ from 'lodash'

export function getTemplateURL(preprocessedData: any) {

    // let template:any = {} 
    // if (preprocessedData.verb === "LAUNCH") {
    //     return "./template/welcome.json"
    // }
    let templateURL:string = ""
    switch(preprocessedData.verb.toLowerCase()){
        case 'launch':
            templateURL = 'https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/splashScreen.json'
            break
        case 'categoryintent':
            templateURL = 'https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/categoryScreen.json'
            break
        default:
            templateURL = 'https://voicegamesdata.s3.amazonaws.com/triviaRush/templates/splashScreen.json'
            break
    }
    return templateURL
    
    // else {
    //     if(preprocessedData.requestContext.data.arguments[0].EventName == 'StopIntent'){
    //         return './template/THANK_YOU.json'
    //     }
    //     return "./template/quiz_screen.json"
    // }
}


export function getDataSources(preprocessedData: any) {
    let datasourceURL:string = ""
    switch(preprocessedData.verb.toLowerCase()){
        case "launch":
            datasourceURL = './DataSources/welcome.json'
            break
        case "categoryintent":
            datasourceURL = './DataSources/LAUNCH.json'
            break
        default:
            datasourceURL = './DataSources/welcome.json'
            break
    }

    let datasource = require(datasourceURL)
    return datasource



    // let dataSource: any = {}
    // if (preprocessedData.verb === "LAUNCH") {
    //     dataSource = require(`./DataSources/welcome.json`)
    //     return dataSource
    // }
    // else if (preprocessedData.verb === "USER_EVENT") {
    //     let fileName: string = ""
    //     if (preprocessedData.requestContext.data.slots.length > 0) {
    //         let slotValue: string = preprocessedData.requestContext.data.slots[0]['slotValue']
    //         fileName = getFileName(slotValue)

    //         // let slotValue: string = preprocessedData.requestContext.data.slots[0]['slotValue']
    //         // fileName = getFileName(slotValue)
    //         let questionData: any = require(`./DataSources/${fileName}.json`)
    //         let currentQuestion = { ...questionData.data.questions[_.random(0, questionData.data.questions.length - 1)] }
    //         let formattedOptions: any = []
    //         for (let i = 0; i < currentQuestion.options.length; i++) {
    //             let resultingOption = {
    //                 "id": String.fromCharCode(65 + i),
    //                 "option": currentQuestion.options[i]
    //             }

    //             formattedOptions.push(resultingOption)
    //         }
    //         currentQuestion.options = []
    //         currentQuestion.options = formattedOptions

    //         let timer = (_.has(preprocessedData.requestContext.data.arguments[0], "SLOTS.currentTime")) ? (preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime > 0) ? preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime - 1000 : preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime : questionData.data.timer
    //         // let timerTest = (_.has(preprocessedData.requestContext.data.arguments[0], "SLOTS.currentTime")) ? console.log("preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime present") : (_.has(preprocessedData.sessionContext, "sessionAttributes.timerVal")) ? console.log("preprocessedData.sessionContext.sessionAttributes.timerVal is present") : questionData.data.timer || 0

    //         dataSource = {
    //             "data": {
    //                 "categoryColor": questionData.data.categoryColor,
    //                 "questions": currentQuestion,
    //                 "backgroundImageURL": questionData.data.backgroundImageURL,
    //                 // "timer":preprocessedData.requestContext.data.arguments[0]['SLOTS'].currentTime || questionData.data.timer,
    //                 "timer": timer,
    //                 "backgroundVideoURL": "https://voicegamesdata.s3.amazonaws.com/trivia-verse/background_video.mp4",
    //                 "category": questionData.data.category
    //             }
    //         }

    //         console.log("----------------------53. business.ts formatted dataSource---------------------", JSON.stringify(dataSource))
    //     }
    //     else if(preprocessedData.requestContext.data.arguments[0].EventName == 'StopIntent'){
    //         console.log("-----------------------------64. StopIntent occurred--------------")
    //         dataSource = require('./DataSources/THANK_YOU.json')
    //         console.log("----------------66. business.ts--------------", dataSource)
    //     }
    //     return dataSource
    // }
    // else if (preprocessedData.verb === "UNKNOWN") {
    //     return undefined
    // }
    // return `./DataSources/${preprocessedData.verb}.json`
}

function getFileName(slotValue: string) {
    let fileName: string = ""
    if (slotValue !== undefined) {
        switch (slotValue.toLowerCase()) {
            case "black clover":
                fileName = "BC_HOME"
                break;
            case "attack on titan":
                fileName = "AOT_HOME"
                break;
            case "chainsawman":
                fileName = "CM_HOME"
                break;
            case "my hero academia":
                fileName = "MHA_HOME"
                break;
            case "launch":
                fileName = "LAUNCH"
                break
            default:
                fileName = "BC_HOME"
                break;
        }
    }
    return fileName
}

export function getOutputSpeech(preprocessedData: any) {
    let slotValue: string = preprocessedData.requestContext.data.slots[0]['slotValue'] || preprocessedData.verb
    let fileName = getFileName(slotValue)
    let outputSpeech = ""
    switch (fileName) {
        case "LAUNCH":
            outputSpeech = "<speak><audio src='https://voicegamesdata.s3.amazonaws.com/tb/prodV2/en-US/sound-files/quiz-welcome.mp3'></audio>Hello, Welcome to Trivia Rush...</speak>";
            // outputSpeech = "Who is main character in Black Clover?"
            break;
        case "BC_HOME":
            outputSpeech = "<speak>So you know Black Clover better, lets check</speak>";
            break;
        case "AOT_HOME":
            outputSpeech = "<speak>Okay someone is Attack on Titan fan, lets see</speak>";
            break;
        case "CM_HOME":
            outputSpeech = "<speak>Chainsaw man huh, here are your questions</speak>";
            break;
        case "MHA_HOME":
            outputSpeech = "<speak>Test your My Hero Academia knowledge</speak>";
            break;
        default:
            outputSpeech = "<speak>Oops, Something went wrong.</speak>"
            break
    }

    return outputSpeech
}

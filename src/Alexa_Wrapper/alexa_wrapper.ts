import { businessHandler } from "../business/BusinessHandler"
import { postProcessingHandler } from "../post_processing/PostprocessingHandler"
import { preprocessingHandler } from "../preprocessing"

export async function alexaWrapper(event: any): Promise<any> {
    
    let preprocessedData = await preprocessingHandler(event)
    console.log("-----------8. alexa_wrapper.ts preprocessedData----------", JSON.stringify(preprocessedData))
    
    let businessOutput = await businessHandler(preprocessedData)
    console.log("-----------11. alexa_wrapper.ts businessOutput----------", JSON.stringify(businessOutput))
    
    let alexaResponse = await postProcessingHandler(businessOutput)
    console.log("-----------14. alexa_wrapper.ts postProcessedData----------", JSON.stringify(alexaResponse))

    return alexaResponse
}

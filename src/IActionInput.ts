export interface IActionInput {
    verb: string
    platForm: string   // Alexa || Mobile
    userContext: IUserContext
    sessionContext: ISessionContext
    deviceContext: IDeviceContext
    requestContext: IRequestContext
}

export interface IUserContext {
    userId: string
    inSkillProducts: any[]
    userAttributes: any
}

export interface ISessionContext {
    sessionId: string
    sessionAttributes: any
    apiAccessToken: string
    apiEndpoint: string
    skillId: string
    isNewSession: boolean
}

export interface IDeviceContext {
    deviceId: string
    shape: string
    mode: string
    width: number
    height: number
    dpi: number
    supportedInterfaces: string[]
    isDisplayEnabled: boolean
}

export interface IRequestContext {
    requestId: string
    timestamp: string
    locale: string
    data: { [key: string]: any }
}

export enum AlexaInterfaces {
    APL="APL",
    AUDIO_PLAYER="AUDIO_PLAYER"
}
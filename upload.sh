rm -rf dist/
npm run build
mkdir ./dist/src/post_processing/template
mkdir ./dist/src/business/DataSources
cp -r ./src/post_processing/template ./dist/src/post_processing
cp -r ./src/business/DataSources ./dist/src/business
npm run upload

import AnswerAction from "./src/business/Actions/AnswerAction";
import CategoryAction from "./src/business/Actions/CategoryAction";
import LaunchAction from "./src/business/Actions/LaunchAction";
import QuizAction from "./src/business/Actions/QuizAction";
import RepeatAction from "./src/business/Actions/RepeatAction";
import TimeoutAction from "./src/business/Actions/TimeoutAction";
import UserEventAction from "./src/business/Actions/UserEventAction";
import YesAction from "./src/business/Actions/YesAction";

export let IntentMap:any = {
    LAUNCH : new LaunchAction(),
    USER_EVENT:new UserEventAction(),
    ANSWER_INTENT:new AnswerAction(),
    CHOOSE_CATEGORY_INTENT:new CategoryAction(),
    YES_INTENT:new YesAction(),
    QUIZ_INTENT:new QuizAction(),
    TIMEOUT_INTENT:new TimeoutAction(),
    UNKNOWN:new RepeatAction(),
    REPEAT:new RepeatAction()
}
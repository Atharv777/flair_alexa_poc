import _ from "lodash"
import { alexaWrapper } from "./src/Alexa_Wrapper/alexa_wrapper"


export const handler = async(event:any, context:any) => {
    console.log("-------7. index.ts Received context from Alexa => ", JSON.stringify(event))
    let response = await alexaWrapper(event)

    return JSON.stringify(response)
}